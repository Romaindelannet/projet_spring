# Projet_Spring

## Pré-requis
- Pour que le projet puisse fonctionner il faut dans un premier temps  installer java :

        sudo apt install openjdk8-jdk
- Il faut également installer mysql pour avoir une base de données :

        sudo apt install mysql-server
- Il faut maintenant lancer mysql :

        sudo mysql --password

    `mot de passe : root`

- Une fois installer et mysql lancé, il faut créer un utilisateur, pour le projet :

        create user 'springuser'@'localhost' identified by 'formation'
- Puis créer la base de données :

        create database spring_db

- Ensuite, il faut donner les droits à l'utilisateur pour gérer la base de données :

        grant all privileges on spring_db.* to 'springuser'@'localhost';

Une fois ces pré-requis fait, il ne reste plus qu'à lancer l'application.

## Lancement de l'application

- Il faut pour commencer utiliser votre IDE afin d'éxecuter le code de l'application. Une fois exécuté, il faut vous rendre à l'adresse :

        localhost:8080

- Sur la page d'acceuil on peut ajouter un évenement, un participant ou encore voir la liste  des participants et l'évenement au quel il participe.

- Lorsqu'un choix est séléctionné il faut retourner à la page d'acceuil pour faire un autre choix.