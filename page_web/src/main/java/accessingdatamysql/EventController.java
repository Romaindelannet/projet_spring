package accessingdatamysql;

import java.util.ArrayList;

//import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import antlr.collections.List;

@Controller // This means that this class is a Controller
//@RequestMapping(path="/event", method = RequestMethod.POST) // This means URL's start with /demo (after Application path)

public class EventController {
	@Autowired // This means to get the bean called userRepository
	private EventRepository eventRepository;

	@PostMapping(path="/addEvent") // Map ONLY POST Requests
	public @ResponseBody String addNewUser (@RequestParam String name) {

	  Evenement ev = new Evenement();
	  ev.setName(name);
	  eventRepository.save(ev);
	  return "Ajout d'un évenement";
	}
	 
	@GetMapping(path="/Event")
	public String getAllEvent(Model model) {
		
	    return "AddEvent";
	}
	

	
}
